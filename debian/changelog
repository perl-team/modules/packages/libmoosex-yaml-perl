libmoosex-yaml-perl (0.05-2) UNRELEASED; urgency=medium

  [ gregor herrmann ]
  * Add debian/upstream/metadata.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libtest-simple-perl.

 -- gregor herrmann <gregoa@debian.org>  Tue, 11 Feb 2020 16:34:15 +0100

libmoosex-yaml-perl (0.05-1) unstable; urgency=medium

  * Import upstream version 0.05.
  * Drop LoadBlessed.patch which was taken from a dev release
    for this new upstream version.

 -- gregor herrmann <gregoa@debian.org>  Mon, 10 Feb 2020 22:57:34 +0100

libmoosex-yaml-perl (0.04-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * Update (build) dependencies on libyaml-*.
  * Add patch LoadBlessed.patch from upstream development release which
    sets $LoadBlessed for compatibility with the new defaults of the
    various YAML* modules. Thanks to Tina Müller for the quick fix.
    (Closes: #950997)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * debian/watch: use uscan version 4.
  * Refer to specific version of license GPL-1+.

 -- gregor herrmann <gregoa@debian.org>  Mon, 10 Feb 2020 17:13:24 +0100

libmoosex-yaml-perl (0.04-2) unstable; urgency=medium

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Update Test::use::ok build dependency.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove franck cuny from Uploaders. Thanks for your work!
  * Add Testsuite declaration for autopkgtest-pkg-perl.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Declare that the package does not need (fake)root to build
  * Reorder libtest-simple-perl et al. build dependencies

 -- Niko Tyni <ntyni@debian.org>  Sat, 31 Mar 2018 14:21:49 +0300

libmoosex-yaml-perl (0.04-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release (closes: #583486).

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ gregor herrmann ]
  * Convert to source format 3.0 (quilt).
  * Minimize debian/rules.
  * debian/copyright: update formatting and packaging info.
  * Set Standards-Version to 3.8.4 (no changes).
  * Add /me to Uploaders.
  * Bump libsub-exporter-perl (build) dependency to >= 0.982.

 -- gregor herrmann <gregoa@debian.org>  Fri, 28 May 2010 00:49:21 +0200

libmoosex-yaml-perl (0.03-1) unstable; urgency=low

  * Initial Release (Closes: #531402).

 -- franck cuny <franck@lumberjaph.net>  Sun, 07 Jun 2009 16:01:34 +0200
